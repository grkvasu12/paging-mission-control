package com.exam.pmcontrol.controller;

import com.exam.pmcontrol.model.MessageTO;
import com.exam.pmcontrol.model.TelemetryData;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
public class PMController {

    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/json")
    public String uploadFile(@RequestParam("file") MultipartFile inputFile) throws IOException {
        String responseString="";
        List<TelemetryData> telemetryDataList = new ArrayList<>();
        List<MessageTO> messageTOList = new ArrayList<>();
        Scanner scanner = new Scanner(inputFile.getInputStream());
        while(scanner.hasNextLine()){
            String[] line = scanner.nextLine().trim().split("[|]");
            telemetryDataList.add(new TelemetryData(line[0],Integer.parseInt(line[1]),toDouble(line[2]),toDouble(line[3]),
                    toDouble(line[4]),toDouble(line[5]),toDouble(line[6]),line[7]));
        }
        Integer count =0;
        for(int i=0; i<telemetryDataList.size(); i++){
            TelemetryData showData =  telemetryDataList.get(i);

            for(int j=0; j<telemetryDataList.size(); j++){
                TelemetryData dataForComparison =  telemetryDataList.get(j);
                    if(dataForComparison.getSatelliteId()== showData.getSatelliteId()){
                        count += compareData(showData, dataForComparison);
                    }
                if(count==3){
                    messageTOList.add(new MessageTO(telemetryDataList.get(i)));
                    break;
                }
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        responseString = mapper.writeValueAsString(messageTOList);
        System.out.println("\n\n\n************************************************************\n");
        System.out.println(responseString);
        System.out.println("************************************************************");

        return responseString;
    }

    private Integer compareData(TelemetryData showData, TelemetryData dataForComparison) {
        int count = 0;

        if(inTimeLimit(showData.getTimestamp(), dataForComparison.getTimestamp()))
            return count + checkVoltAndLimit(showData, dataForComparison);
        return 0;
    }

    public Double toDouble(String str){
        return Double.parseDouble(str);
    }
    private Integer checkVoltAndLimit(TelemetryData showData, TelemetryData dataForComparison) {
        String showComponent = showData.getComponent();
        String comparisonComponent = dataForComparison.getComponent();
        int returnCount = 0;

        if (showComponent.equals(comparisonComponent) && showComponent.equals("TSTAT") && showData.getSatelliteId().equals(dataForComparison.getSatelliteId())) {
            returnCount = temperatureCheck(dataForComparison.getRawValue(), dataForComparison.getRedHighLimit());
        }
        if (showComponent.equals(comparisonComponent) && showComponent.equals("BATT") && showData.getSatelliteId().equals(dataForComparison.getSatelliteId())) {
            returnCount = voltageCheck(dataForComparison.getRawValue(), dataForComparison.getRedLowLimit());

        }
        return returnCount;
    }

    public Integer temperatureCheck(Double rawValue, Double highRedLimit) {
        return (rawValue > highRedLimit) ? 1 :0;
    }

    public Integer voltageCheck(Double rawValue, Double lowRedLimit) {
        return (rawValue < lowRedLimit) ? 1 :0;
    }

    public boolean inTimeLimit(String time1, String time2) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
        LocalDateTime dateTime1 = LocalDateTime.parse(time1, formatter);
        LocalDateTime dateTime2 = LocalDateTime.parse(time2, formatter);
        LocalDateTime minimumTime = dateTime1.minusMinutes((long) 5);

        int result = dateTime2.compareTo(minimumTime);
        if (result > -1) {
            return true;
        }
        return false;
    }


}
