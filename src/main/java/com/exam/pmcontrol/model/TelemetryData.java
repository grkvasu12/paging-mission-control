package com.exam.pmcontrol.model;

public class TelemetryData {

    String timestamp;
    Integer satelliteId;
    Double redHighLimit;
    Double yellowHighLimit;
    Double yelloeLowLimit;
    Double redLowLimit;
    Double rawValue;
    String component;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(Integer satelliteId) {
        this.satelliteId = satelliteId;
    }

    public Double getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(Double redHighLimit) {
        this.redHighLimit = redHighLimit;
    }

    public Double getYellowHighLimit() {
        return yellowHighLimit;
    }

    public void setYellowHighLimit(Double yellowHighLimit) {
        this.yellowHighLimit = yellowHighLimit;
    }

    public Double getYelloeLowLimit() {
        return yelloeLowLimit;
    }

    public void setYelloeLowLimit(Double yelloeLowLimit) {
        this.yelloeLowLimit = yelloeLowLimit;
    }

    public Double getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(Double redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public Double getRawValue() {
        return rawValue;
    }

    public void setRawValue(Double rawValue) {
        this.rawValue = rawValue;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public TelemetryData(String timestamp, Integer satelliteId, Double redHighLimit, Double yellowHighLimit, Double yelloeLowLimit, Double redLowLimit, Double rawValue, String component) {
        this.timestamp = timestamp;
        this.satelliteId = satelliteId;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yelloeLowLimit = yelloeLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
    }

}
