package com.exam.pmcontrol.model;

import java.util.Objects;

public class MessageTO {

       Integer satelliteId;
       String severity;
       String component;
       String timestamp;

    public MessageTO(Integer satelliteId, String severity, String component, String timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }

    public MessageTO(TelemetryData telemetryData) {
        this.satelliteId=telemetryData.getSatelliteId();
            if (telemetryData.getComponent().equals("TSTAT")) {
                    this.severity = "RED HIGH";
            } else {
                    this.severity = "RED LOW";
            }

            this.component = telemetryData.getComponent();
            this.timestamp=telemetryData.getTimestamp();
    }

    public Integer getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(Integer satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageTO messageTO = (MessageTO) o;
        return Objects.equals(satelliteId, messageTO.satelliteId) &&
                Objects.equals(severity, messageTO.severity) &&
                Objects.equals(component, messageTO.component) &&
                Objects.equals(timestamp, messageTO.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, severity, component, timestamp);
    }
}
