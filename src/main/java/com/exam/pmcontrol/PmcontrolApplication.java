package com.exam.pmcontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PmcontrolApplication {

    public static void main(String[] args) {
        SpringApplication.run(PmcontrolApplication.class, args);
    }

}
